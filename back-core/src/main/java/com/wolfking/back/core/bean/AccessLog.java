/**
 * 
 */
package com.wolfking.back.core.bean;

import java.io.Serializable;
import java.util.Date;

import com.wolfking.back.core.annotation.mybatis.MyColumn;
import com.wolfking.back.core.annotation.mybatis.MyId;
import com.wolfking.back.core.annotation.mybatis.MyTable;

/**
 * 访问日志
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午2:21:27
 * @版权 归wolfking所有 @SuppressWarnings("serial")
 */
@MyTable("sys_log")
public class AccessLog implements Serializable {

	private static final long serialVersionUID = -5184841304494566527L;
	@MyId
	private String id;
	@MyColumn
	private String type; // 日志类型（1：接入日志；2：错误日志）
	@MyColumn
	private String title; // 日志标题
	@MyColumn("remote_addr")
	private String remoteAddr; // 操作用户的IP地址
	@MyColumn("request_uri")
	private String requestUri; // 操作的URI
	@MyColumn
	private String method; // 操作的方式
	@MyColumn
	private String params; // 操作提交的数据
	@MyColumn("user_agent")
	private String userAgent; // 操作用户代理信息
	@MyColumn
	private String exception; // 异常信息
	@MyColumn("create_date")
	private Date createDate; // 创建时间
	@MyColumn("create_by")
	private String createBy; // 访问的人

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            要设置的 id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            要设置的 type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            要设置的 title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return remoteAddr
	 */
	public String getRemoteAddr() {
		return remoteAddr;
	}

	/**
	 * @param remoteAddr
	 *            要设置的 remoteAddr
	 */
	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	/**
	 * @return requestUri
	 */
	public String getRequestUri() {
		return requestUri;
	}

	/**
	 * @param requestUri
	 *            要设置的 requestUri
	 */
	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	/**
	 * @return method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method
	 *            要设置的 method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return params
	 */
	public String getParams() {
		return params;
	}

	/**
	 * @param params
	 *            要设置的 params
	 */
	public void setParams(String params) {
		this.params = params;
	}

	/**
	 * @return userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent
	 *            要设置的 userAgent
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return exception
	 */
	public String getException() {
		return exception;
	}

	/**
	 * @param exception
	 *            要设置的 exception
	 */
	public void setException(String exception) {
		this.exception = exception;
	}

	/**
	 * @return createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate
	 *            要设置的 createDate
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return createBy
	 */
	public String getCreateBy() {
		return createBy;
	}

	/**
	 * @param createBy
	 *            要设置的 createBy
	 */
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

}
