package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 用户数据远程调用的client
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月23日 下午5:22:34
 * @copyright wolfking
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface UserFeignClient {
	@POST
	@Path("/wolfking/user")
	ResponseEntity<Boolean> addUser(@RequestBody(required = true) User user);

	@PUT
	@Path("/wolfking/user")
	ResponseEntity<Boolean> updateUser(@RequestBody(required = true) User user);

	@GET
	@Path("/wolfking/user")
	ResponseEntity<List<User>> getAllUser();

	@DELETE
	@Path("/wolfking/user/{id}")
	ResponseEntity<Boolean> deleteUser(@PathParam("id") String userId);

	@POST
	@Path("/wolfking/user/page")
	ResponseEntity<PageInfo<User>> pageUser(@RequestBody(required = true) User user,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	@GET
	@Path("/wolfking/user/modifyPwd/{id}")
	ResponseEntity<Boolean> modifyPwd(@PathParam("id") String id, @HeaderParam("oldPassword") String oldPassword,
			@HeaderParam("newPassword") String newPassword);

	@GET
	@Path("/wolfking/user/{id}")
	ResponseEntity<User> getUser(@PathParam("id") String userId);

	@POST
	@Path("/wolfking/user/seleteAccuracy")
	ResponseEntity<List<User>> seleteAccuracy(@RequestBody(required = true) User user);

	@GET
	@Path("/wolfking/user/role/{roleId}")
	ResponseEntity<List<User>> getUserListByRoleId(@PathParam("roleId") String roleId);
}
