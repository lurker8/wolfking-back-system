package com.wolfking.back.core.swagger;

import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jersey.JerseyApiReader;
import com.wordnik.swagger.model.ApiInfo;
import com.wordnik.swagger.reader.ClassReaders;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * swagger的配置
 * Created by wolfking(赵伟伟)
 * Created on 2016/12/17 17:11
 * Mail zww199009@163.com
 */
@Configuration
public class SwaggerConfigurer extends WebMvcConfigurerAdapter {
	
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(new String[]{"/**"}).addResourceLocations(new String[]{"classpath:/static/", "classpath:/templates/", "classpath:/META-INF/resources/webjars/"});
        super.addResourceHandlers(registry);
    }

    public static void initSwagger(String title, String description) {
    	if(StringUtils.isBlank(description))
    		description = title;
        SwaggerConfig config = ConfigFactory.config();
        config.setBasePath("/wolfking");
        config.setApiVersion("1.0.0");
        config.setApiInfo(new ApiInfo(title, "<a href=\"/api\" target=\"_blank\">" + description + "</a>", null, null, null, null));
        ScannerFactory.setScanner(new DefaultJaxrsScanner());
        ClassReaders.setReader(new JerseyApiReader());
    }
}
