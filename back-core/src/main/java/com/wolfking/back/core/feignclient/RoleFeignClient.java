package com.wolfking.back.core.feignclient;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.wolfking.back.core.bean.PageInfo;
import com.wolfking.back.core.bean.Role;
import com.wolfking.back.core.bean.User;
import com.wolfking.back.core.feign.JerseyFeignConfig;

/**
 * 角色的远程调用feignclient
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年5月3日下午5:13:57
 * @版权 归wolfking所有
 */
@FeignClient(name = "sso-auth-service", configuration = { JerseyFeignConfig.class })
public interface RoleFeignClient {
	@GET
	@Path("/wolfking/role/{id}")
	ResponseEntity<Role> getRole(@PathParam("id") String id);

	@GET
	@Path("/wolfking/role")
	ResponseEntity<List<Role>> getAllRole();

	@POST
	@Path("/wolfking/role/page")
	ResponseEntity<PageInfo<Role>> pageRole(@RequestBody(required = true) Role role,
			@HeaderParam("pageNum") int pageNum, @HeaderParam("pageSize") int pageSize);

	@PUT
	@Path("/wolfking/role")
	ResponseEntity<Boolean> updateRole(@RequestBody(required = true) Role role);

	@DELETE
	@Path("/wolfking/role/{id}")
	ResponseEntity<Boolean> deleteRole(@PathParam("id") String id);

	@POST
	@Path("/wolfking/role")
	ResponseEntity<Boolean> addRole(@RequestBody(required = true) Role role);

	@POST
	@Path("/wolfking/role/selectAccuracy")
	ResponseEntity<List<Role>> selectAccuracy(@RequestBody(required = true) Role role);

	@DELETE
	@Path("/wolfking/role/outrole/{roleId}/{userId}")
	ResponseEntity<Boolean> outrole(@PathParam("roleId") String roleId, @PathParam("userId") String userId);

	@GET
	@Path("/wolfking/role/assginrole/{roleId}/{userId}")
	ResponseEntity<User> assginrole(@PathParam("roleId") String roleId, @PathParam("userId") String userId);

}
