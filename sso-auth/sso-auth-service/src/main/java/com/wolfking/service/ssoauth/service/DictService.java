package com.wolfking.service.ssoauth.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.DictMapper;

/**
 * 字典的服务
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月25日下午3:23:48
 * @版权 归wolfking所有
 */
@Service
public class DictService extends BaseService<DictMapper, Dict> {

	@Transactional(readOnly = true)
	public List<String> getAllDictType() {
		return mapper.getAllType();
	}
}
