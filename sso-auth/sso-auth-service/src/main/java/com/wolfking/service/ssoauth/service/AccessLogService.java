/**
 * 
 */
package com.wolfking.service.ssoauth.service;

import org.springframework.stereotype.Service;

import com.wolfking.back.core.bean.AccessLog;
import com.wolfking.back.core.mybatis.BaseService;
import com.wolfking.service.ssoauth.mapper.AccessLogMapper;

/**
 * 访问日志的mapper
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月27日下午2:29:15
 * @版权 归wolfking所有
 */
@Service
public class AccessLogService extends BaseService<AccessLogMapper, AccessLog> {

}
