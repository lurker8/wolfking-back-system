package com.wolfking.service.ssoauth.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.wolfking.back.core.bean.Dict;
import com.wolfking.back.core.mybatis.BaseMapper;

/**
 * 字典的mapper映射
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月25日下午3:21:27
 * @版权 归wolfking所有
 */
@Mapper
public interface DictMapper extends BaseMapper<Dict> {
	/**
	 * 查询所有的字典的类别
	 * 
	 * @return
	 */
	@Select("select distinct(type) from sys_dict")
	List<String> getAllType();
}
