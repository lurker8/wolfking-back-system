package com.wolfking.web.ssoauth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wolfking.back.core.util.CookieUtil;

/**
 * theme的controller
 * <P>
 * 主要是设置cookie的一个值
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月26日上午9:38:00
 * @版权 归wolfking所有
 */
@Controller
public class ThemeController {
	@RequestMapping(value = "/theme/{theme}", method = RequestMethod.GET)
	public String cookie(@PathVariable String theme, @RequestParam String url,
			@RequestParam(required = false, defaultValue = "theme") String cookie, HttpServletRequest request,
			HttpServletResponse response) {
		CookieUtil.setCookie(request, response, cookie, theme);
		return "redirect:" + url;
	}
}
