package com.wolfking.web.ssoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import com.wolfking.back.core.config.SsoConfig;

/**
 * sso-web的启动类
 * <P>
 * 
 * @author wolfking@赵伟伟
 * @mail zww199009@163.com
 * @创作日期 2017年4月19日下午5:42:30
 * @版权 归wolfking所有
 */
@Controller
@EnableEurekaClient
@SpringBootApplication
@EnableConfigurationProperties(SsoConfig.class)
@ComponentScan(basePackages = { "com.wolfking" })
@EnableFeignClients(basePackages = "com.wolfking")
@ServletComponentScan(basePackages = { "com.wolfking" })
public class SsoAuthWeb {

	@LoadBalanced
	private RestTemplate restTemplate;

	public static void main(String[] args) {
		SpringApplication.run(SsoAuthWeb.class, args);
	}
}
